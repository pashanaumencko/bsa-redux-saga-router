import { 
    FETCH_USERS,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    ADD_USER,
    ADD_USER_SUCCESS,
    ADD_USER_FAILURE,
    UPDATE_USER,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE
} from "../constants/user";


const fetchUsersAction = () => ({ type: FETCH_USERS });

const addUserAction = (user) => ({ type: ADD_USER, user });

const updateUserAction = (user) => ({ type: UPDATE_USER, user });

const deleteUserAction = (id) => ({ type: DELETE_USER, id });

export {
    fetchUsersAction,
    addUserAction,
    updateUserAction,
    deleteUserAction,
};
