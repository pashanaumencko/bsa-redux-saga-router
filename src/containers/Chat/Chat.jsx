import React from 'react';
import { connect } from 'react-redux';
// import * as actions from '../../actions/message';
import { fetchMessagesAction } from '../../actions/message';
import Header from '../../components/Header/Header';
import MessageList from '../../components/MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import Spinner from '../../components/Spinner/Spinner';
import ErrorLabel from '../../components/ErrorLabel/ErrorLabel';


import './Chat.css';

class Сhat extends React.Component {
  componentDidMount() {
    this.props.fetchMessagesAction();
    console.log(this.props.messages);
  }

  getHeader() {
    const messages = this.props.messages;
    return <Header messages={messages} />;
  }

  getMessageList() {
    const messages = this.props.messages;
    return <MessageList messages={messages}  />;
  }

  getMessageInput() {
    return <MessageInput />
  }

  render() {
    if(this.props.fetchMessagesLoading) {
      console.log('loading');
      return <Spinner />; 
    } else if (this.props.fetchMessagesError) {
      console.log('error');
      return <ErrorLabel message={'fetching'} />;
    } else {
      console.log('render');
      return (
      <div className="container">
          <div className="chat">
            {this.getHeader()}
            {this.getMessageList()}
            {this.getMessageInput()}
          </div>
        </div>
      );
    }
    
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    messages: state.messageReducer.messages,
    fetchMessagesLoading: state.messageReducer.fetchMessagesLoading,
    fetchMessagesError: state.messageReducer.fetchMessagesError,
  };
};

const mapDispatchToProps = {
  fetchMessagesAction, 
};


export default connect(mapStateToProps, mapDispatchToProps)(Сhat);
