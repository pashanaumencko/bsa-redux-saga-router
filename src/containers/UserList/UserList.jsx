import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/message';
import { getMessagesAction } from '../../actions/message';
import Spinner from '../../components/Spinner/Spinner';


import './UserList.css';
import UserItem from '../../components/UserItem/UserItem';

class UserList extends React.Component {



  render() {
    return (
      <div className="container bootstrap snippet">
        <div className="row">
          <div className="col-lg-12">
            <div className="main-box no-header clearfix">
              <div className="main-box-body clearfix">
                <div className="table-responsive">
                  <table className="table user-list">
                    <thead>
                      <tr>
                        <th><span>User</span></th>
                        <th><span>Created</span></th>
                        <th className="text-center"><span>Status</span></th>
                        <th><span>Email</span></th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <UserItem />
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserList;