import React from 'react';
import Chat from '../Chat/Chat';
import UserList from '../UserList/UserList';
import MessageEditor from '../MessageEditor/MessageEditor';
import { Switch, Route } from 'react-router-dom';

const App = (props) => {
  return (
    <div>
      <Switch>
        <Route exact path='/' component={Chat} />
        <Route exact path="/message/:id" component={MessageEditor} />
				<Route exact path="/user" component={UserList} />
				{/* <Route path="/user/:id" component={UserPage} /> */}
      </Switch>
    </div>
  );
};

export default App;