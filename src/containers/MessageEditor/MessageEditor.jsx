import React from 'react';
import { connect } from 'react-redux';
import Spinner from '../../components/Spinner/Spinner';
import { updateMessageAction, fetchMessageAction } from '../../actions/message';

import './MessageEditor.css';

class MessageEditor extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.fetchMessageAction(this.props.match.params.id)
    }
  }

  onSubmit(event) {
    event.preventDefault();
    const editedMessageText = event.target.elements.editMessage.value;
    const messageObj = this.props.message;
    messageObj.message = editedMessageText
    if (editedMessageText) {
      this.props.updateMessageAction(messageObj);
      this.props.history.push('/');
    }
  }

  onCancel() {
    this.props.history.push('/');
  }

  render() {
    if (this.props.fetchMessageLoading) {
      return <Spinner />;
    } else if (this.props.fetchMessageError) {
      return <span>Something has happened unexpectedly while fetching</span>
    } else {
      return (
        <div className="container">
          <form id="editMessageForm" className="edit-message-form" onSubmit={this.onSubmit}>
            <h5>Edit message</h5>
            <div className="form-group">
              <label htmlFor="message-text" className="col-form-label">Message:</label>
              <textarea className="form-control" name="editMessage" defaultValue={this.props.message.message} ></textarea>
            </div>
            <button type="button" className="btn btn-secondary mr-3" onClick={this.onCancel}>Cancel</button>
            <button type="submit" form="editMessageForm" className="btn btn-primary">Edit message</button>
          </form>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    message: state.messageReducer.message,
    updateMessageLoading: state.messageReducer.updateMessageLoading,
    updateMessageError: state.messageReducer.updateMessageError,
    fetchMessageLoading: state.messageReducer.fetchMessageLoading,
    fetchMessageError: state.messageReducer.fetchMessageError,
  };
};

const mapDispatchToProps = {
  updateMessageAction,
  fetchMessageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);