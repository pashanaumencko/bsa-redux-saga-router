import { 
  FETCH_MESSAGES, 
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  FETCH_MESSAGE,
  FETCH_MESSAGE_SUCCESS,
  FETCH_MESSAGE_FAILURE,
  ADD_MESSAGE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAILURE,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_FAILURE,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAILURE
} from "../constants/message";

const initialState = {
  messages: [],
  message: {},
  fetchMessagesLoading: true,
  fetchMessagesError: false,
  fetchMessageLoading: false,
  fetchMessageError: true,
  addMessageLoading: false,
  addMessageError: false,
  updateMessageLoading: true,
  updateMessageError: false,
  deleteMessageLoading: true,
  deleteMessageError: false,
};

const messageReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_MESSAGES:
      return Object.assign({}, state, { messages: [], fetchMessagesLoading: true, fetchMessagesError: false });
    case FETCH_MESSAGES_SUCCESS:
      return Object.assign({}, state, { messages: action.messages, fetchMessagesLoading: false });
    case FETCH_MESSAGES_FAILURE:
      return Object.assign({}, state, { fetchMessagesError: true, fetchMessagesLoading: false  });
    case FETCH_MESSAGE:
      return Object.assign({}, state, { message: {}, fetchMessageLoading: true, fetchMessageError: false });
    case FETCH_MESSAGE_SUCCESS:
      return Object.assign({}, state, { message: action.message, fetchMessageLoading: false });
    case FETCH_MESSAGE_FAILURE:
      return Object.assign({}, state, { fetchMessageError: true, fetchMessageLoading: false });
    case ADD_MESSAGE:
      return Object.assign({}, state, { messages: [], addMessageLoading: true, addMessageError: false });
    case ADD_MESSAGE_SUCCESS:
        return Object.assign({}, state, { addMessageLoading: false });
    case ADD_MESSAGE_FAILURE:
        return Object.assign({}, state, { addMessageError: true, addMessageLoading: false });
    case UPDATE_MESSAGE:
      return Object.assign({}, state, { messages: [], updateMessageLoading: true, updateMessageError: false });
    case UPDATE_MESSAGE_SUCCESS:
      return Object.assign({}, state, { updateMessageLoading: false });
    case UPDATE_MESSAGE_FAILURE:
      return Object.assign({}, state, { updateMessageError: true, updateMessageLoading: false });
    case DELETE_MESSAGE:
      return Object.assign({}, state, { messages: [], deleteMessageLoading: true, deleteMessageError: false });
    case DELETE_MESSAGE_SUCCESS:
        return Object.assign({}, state, { deleteMessageLoading: false });
    case DELETE_MESSAGE_FAILURE:
        return Object.assign({}, state, { deleteMessageError: true, deleteMessageLoading: false });
    default:
      return state;
  }
}


export default messageReducer;