import uuidv4 from 'uuid/v4';

const getUniqueId = () => uuidv4();

const API_URL = 'http://localhost:8000';

export { getUniqueId, API_URL };