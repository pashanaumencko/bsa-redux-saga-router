import React from 'react';
import moment from 'moment';

import './Header.css';

class Header extends React.Component {
  getLastMessageTime() {
    const sortedMessages = this.props.messages;
    const timeFormat = 'YYYY-MM-DD HH:mm';
    return sortedMessages.length ? moment(sortedMessages[0].created_at, timeFormat).format('Do MMMM HH:mm') : null;
  }

  getUsersCount() {
    const messages = this.props.messages
    return messages
      .map(message => message.user)
      .filter((user, index, users) => users.indexOf(user) === index).length;
  }

  render() {
    const usersCount = this.getUsersCount();
    const lastTimeAt = this.getLastMessageTime();
    const messagesCount = this.props.messages.length;

    return (
      <header className='chat-header'>
          <div className='chat-info'>
              <div className='header-logo'>My Chat</div>
              <div className='header-participants'>{usersCount} participants</div>
              <div className='header-messages'>{messagesCount} messages</div>
          </div>
          <div>
              <div className='header-last-message'>last message at {lastTimeAt}</div>
          </div>
      </header>
    );
  }
}

export default Header;