import React from 'react';


const UserItem = (props) => {
  return (
    <tr>
      <td>
        <img src="https://bootdey.com/img/Content/user_1.jpg" alt="" />
        <a href="#" className="user-link">Full name 1</a>
        <span className="user-subhead">Member</span>
      </td>
      <td>2013/08/12</td>
      <td className="text-center">
        <span className="label label-default">pending</span>
      </td>
      <td>
        <a href="#">marlon@brando.com</a>
      </td>
      <td style={{ "width": "20%" }}>
        <a href="#" className="table-link">
          <span className="fa-stack">
            <i className="fa fa-square fa-stack-2x"></i>
            <i className="fa fa-search-plus fa-stack-1x fa-inverse"></i>
          </span>
        </a>
        <a href="#" className="table-link">
          <span className="fa-stack">
            <i className="fa fa-square fa-stack-2x"></i>
            <i className="fa fa-pencil fa-stack-1x fa-inverse"></i>
          </span>
        </a>
        <a href="#" className="table-link danger">
          <span className="fa-stack">
            <i className="fa fa-square fa-stack-2x"></i>
            <i className="fa fa-trash-o fa-stack-1x fa-inverse"></i>
          </span>
        </a>
      </td>
    </tr>
  );
};

export default UserItem;