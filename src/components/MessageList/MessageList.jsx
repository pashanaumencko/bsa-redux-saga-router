import React from 'react';
import { connect } from 'react-redux';
import Message from './../Message/Message';
import MyMessage from './../MyMessage/MyMessage';
import moment from 'moment';


import './MessageList.css';

const MessageList = (props) => {
  console.log(props);
  return (
    <div className="message-list">
        {getMessageList(props.messages)}
    </div>
  );
};

 const getSortedMessagesByDate = (messages) => {
    return messages
      .sort((firstMessage, secondMessage) => moment.utc(firstMessage.timeStamp).diff(moment.utc(secondMessage.timeStamp)));
  }

  const getMessageList = (messages) => {
    const sortedMessages = getSortedMessagesByDate(messages);

    return sortedMessages.map(message => {
      const { id, user } = message;
      return (user === 'Pavlo') 
        ? <MyMessage 
            key={id} 
            message={message} />
        : <Message 
            key={id} 
            message={message} />;
    });
  }

// const mapStateToProps = (state) => {
//   console.log(state);
//   return {
//     messages: state.messages,
//   };
// };

export default MessageList;
// export default connect(mapStateToProps)(MessageList);