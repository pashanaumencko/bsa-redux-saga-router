import React from 'react';
import { connect } from 'react-redux';
import { addMessageAction } from '../../actions/message';
import moment from 'moment';
import { getUniqueId } from '../../services/services';
import Spinner from '../../components/Spinner/Spinner';
import ErrorLabel from '../../components/ErrorLabel/ErrorLabel';


import './MessageInput.css';


class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    let inputMessage = event.target.elements.messageInput;
    const newMessage = {
      user: 'Pavlo',
      id: getUniqueId(),
      created_at: moment().format('YYYY-MM-DD HH:mm'),
      message: inputMessage.value
    };
    if(inputMessage.value) {
      this.props.addMessageAction(newMessage);
    }
    inputMessage.value = '';
  } 

  render() {
    if(this.props.addMessageLoading) {
      return(
        <div className="type-msg">
          <form className="input-msg-write" onSubmit={this.onSubmit}>
            <textarea type="text" className="write-msg" name="messageInput" placeholder="Type a message" />
            <Spinner />;          
          </form>
        </div>
      );
    } else if (this.props.addMessageError) {

    } else {
      return(
        <div className="type-msg">
          <form className="input-msg-write" onSubmit={this.onSubmit}>
            <textarea type="text" className="write-msg" name="messageInput" placeholder="Type a message" />
            <button className="msg-send-btn" type="submit"><i className="fa fa-paper-plane-o" aria-hidden="true"></i></button>
          </form>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    addMessageLoading: state.messageReducer.addMessageLoading,
    addMessageError: state.messageReducer.addMessageError,
  };
};

const mapDispatchToProps = {
  addMessageAction
};


export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);