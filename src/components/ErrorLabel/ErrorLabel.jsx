import React from 'react';

import './ErrorLabel.css';

const ErrorLabel = (props) => {
  return (
    <div className="alert alert-danger alert-dismissible fade show" role="alert">
      An error ocurred while {props.message}. Please try again.
      <button type="button" className="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
};

export default ErrorLabel;