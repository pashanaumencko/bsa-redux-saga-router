import React from 'react';
import { connect } from 'react-redux';
import { deleteMessageAction } from '../../actions/message';
import Spinner from '../../components/Spinner/Spinner';
import { withRouter } from "react-router-dom";

import './MyMessage.css';

class MyMessage extends React.Component {
  constructor(props) {
    super(props);

    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
  }

  onDeleteClick(event, id) {
    this.props.deleteMessageAction(id);
  }

  onEditClick(event) {
    const { id } = this.props.message;
    this.props.history.push(`/message/${id}`);
  }

  getDeleteButton(messageId) {
    if(this.props.deleteMessageLoading) {
      return <Spinner />; 
    } else if (this.props.deleteMessageError) {
      return ( 
        <div>
          <div className="msg-delete" onClick={event => this.onDeleteClick(event, messageId)}>
            <i className="fa fa-trash"></i>
          </div>
          <span>Something has happened unexpectedly while deleting</span>
        </div>
      );
    } else {
      return (
        <div className="msg-delete" onClick={event => this.onDeleteClick(event, messageId)}>
          <i className="fa fa-trash"></i>
        </div>
      );
    }
  }

  render() {
    const { id:messageId, user:userName, message:messageText, created_at:messageDate } = this.props.message;
    return(
      <div className="outgoing-msg">
        <div className="sent-msg">
          <p>
            <h6>{userName}</h6>
            {messageText}
          </p>
          <div className="msg-info">
            <span className="time-date">{messageDate}</span>
            <div className="msg-actions">
              {this.getDeleteButton(messageId)}
              <div className="msg-edit" onClick={this.onEditClick}>
                <i className="fa fa-edit"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    deleteMessageLoading: state.deleteMessageLoading,
    deleteMessageError: state.deleteMessageError,
  };
};

const mapDispatchToProps = {
  deleteMessageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MyMessage));