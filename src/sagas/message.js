import { call, put, takeEvery, all } from 'redux-saga/effects';
import { 
  FETCH_MESSAGES, 
  FETCH_MESSAGES_SUCCESS,
	FETCH_MESSAGES_FAILURE,
	FETCH_MESSAGE,
	FETCH_MESSAGE_SUCCESS,
	FETCH_MESSAGE_FAILURE,
  ADD_MESSAGE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAILURE,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_FAILURE,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAILURE
} from "../constants/message";
import axios from 'axios';
import { API_URL } from '../services/services';


function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* fetchMessages() {
	try {
		const messages = yield call(axios.get, `${API_URL}/message`);
		yield put({ type: FETCH_MESSAGES_SUCCESS, messages: messages.data });
	} catch (error) {
		yield put({ type: FETCH_MESSAGES_FAILURE });
	}
}

function* watchFetchMessage() {
	yield takeEvery(FETCH_MESSAGE, fetchMessage)
}

export function* fetchMessage(action) {
	try {
		const message = yield call(axios.get, `${API_URL}/message/${action.id}`);
		yield put({ type: FETCH_MESSAGE_SUCCESS, message: message.data });
	} catch (error) {
		yield put({ type: FETCH_MESSAGE_FAILURE });
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* addMessage(action) {
	const newMessage = action.message;

	try {
		yield call(axios.post, `${API_URL}/message`, newMessage);
    yield put({ type: FETCH_MESSAGES });
    yield put({ type: ADD_MESSAGE_SUCCESS });
	} catch (error) {
		yield put({ type: ADD_MESSAGE_FAILURE });
	}
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessage)
}

export function* updateMessage(action) {
	const updatedMessage = action.message;
	const { id } = updatedMessage;
	
	try {
		yield call(axios.put, `${API_URL}/message/${id}`, updatedMessage);
    yield put({ type: FETCH_MESSAGES });
    yield put({ type: UPDATE_MESSAGE_SUCCESS });
	} catch (error) {
		yield put({ type: UPDATE_MESSAGE_FAILURE });
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${API_URL}/message/${action.id}`);
		yield put({ type: FETCH_MESSAGES });
		yield put({ type: DELETE_MESSAGE_SUCCESS });
	} catch (error) {
		yield put({ type: DELETE_MESSAGE_FAILURE });
	}
}


export default function* messagesSagas() {
	yield all([
		watchFetchMessages(),
		watchFetchMessage(),
		watchAddMessage(),
		watchUpdateMessage(),
		watchDeleteMessage()
	])
};