import { call, put, takeEvery, all } from 'redux-saga/effects';
import { 
    FETCH_USERS,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    ADD_USER,
    ADD_USER_SUCCESS,
    ADD_USER_FAILURE,
    UPDATE_USER,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE
} from "../constants/user";
import { API_URL } from '../services/services';

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsers)
}

export function* fetchUsers() {
	try {
		const users = yield call(fetch, `${API_URL}/user`, { method: 'GET' });
        yield put({ type: FETCH_USERS_SUCCESS, users: users.data });
	} catch (error) {
		yield put({ type: FETCH_USERS_FAILURE });
	}
}

function* watchAddUser() {
	yield takeEvery(ADD_USER, addUser)
}

export function* addUser(action) {
	const newUser = action.user;

	try {
		yield call(fetch, `${API_URL}/user`, { method: 'POST', body: newUser });
        yield put({ type: FETCH_USERS });
        yield put({ type: ADD_USER_SUCCESS });
	} catch (error) {
		yield put({ type: ADD_USER_FAILURE });
	}
}

function* watchUpdateUser() {
	yield takeEvery(UPDATE_USER, updateUser)
}

export function* updateUser(action) {
	const updatedUser = action.user;
	const { _id } = updatedUser;
	
	try {
		yield call(fetch, `${API_URL}/user/${_id}`, { method:'PUT', body: updatedUser } );
        yield put({ type: FETCH_USERS });
        yield put({ type: UPDATE_USER_SUCCESS });
	} catch (error) {
		yield put({ type: UPDATE_USER_FAILURE });
	}
}

function* watchDeleteUser() {
	yield takeEvery(DELETE_USER, deleteUser)
}

export function* deleteUser(action) {
	try {
		yield call(fetch, `${API_URL}/user/${action.id}`, { method: 'DELETE' });
        yield put({ type: FETCH_USERS });
        yield put({ type: DELETE_USER_SUCCESS });
	} catch (error) {
		yield put({ type: DELETE_USER_FAILURE });
	}
}


export default function* usersSagas() {
	yield all([
		watchFetchUsers(),
		watchAddUser(),
		watchUpdateUser(),
		watchDeleteUser()
	])
};