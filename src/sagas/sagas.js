import { all } from 'redux-saga/effects';
import messagesSagas from './message';
import usersSagas from './user';

export default function* rootSaga() {
    yield all([
        messagesSagas(),
        usersSagas()
    ]);
};