const typeis = require('type-is');

const IsJSON = (req, res, next) => {
  if (!typeis(req, ['application/json'])) {
    return res.status(400).send(`Incorrect request format`);
  }
  next();
};

module.exports = {
  IsJSON
}