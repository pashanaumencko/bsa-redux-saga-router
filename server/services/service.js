const { Data } = require("../repositories/repository");

class Service {
  static getUsers() {
    const allUsers = Data.getData('./userlist.json');
    return {
      message: 'GET. Get all users',
      data: allUsers
    };
  }

  static getUser(userId) {
    console.log(userId - 1);
    const user = Service.getUsers().data[userId - 1];
    console.log(user, 'User');
    if (!user) {
      return { 
        error: 'Can not GET. User with current id doesn`t exist'
      };
    } else {
      return {
        message: 'GET. Get user by id',
        data: user
      };
    }
  }

  static addUser(user) {
    const { _id } = user;
    console.log(_id);
    if (_id <= 0 || isNaN(_id)) {
      return { 
        error: 'Can not POST. Wrong id number'
      };
    } else if (Service.getUser(Number(_id)).data) {
      return { 
        error: 'Can not POST. User with current id exists'
      };
    } else {
      Data.setData(user, './userlist.json');
      return {
        message: 'POST. Create new user'
      };
    }
  }

  static updateUser(user, id) {
    const { _id } = user
    if (id != _id) {
      return { 
        error: 'Can not PUT. Ids in the query string and in the object don`t match'
      };
    } else if (Service.getUser(id).error) {
      Data.setData(user, './userlist.json');
      return {
        message: 'PUT. Create new user',
        code: 201
      };
    } else {
      Data.updateData(user, './userlist.json');
      return {
        message: 'PUT. Edit existing user',
        code: 200
      };
    }
  }

  static deleteUser(id) {
    if (Service.getUser(id).error) {
      return { 
        error: 'Can not DELETE. User with current id doesn`t exist'
      };
    } else {
      Data.deleteData(id, './userlist.json');
      return {
        message: 'DELETE. Delete existing user'
      };
    }
  };

  static getMessages() {
    const allMessages = Data.getData('./messagelist.json');
    return {
      message: 'GET. Get all messages',
      data: allMessages
    };
  }

  static getMessage(messageId) {
    const message = Service.getMessages().data.find(message => message.id === messageId);
    console.log(message, 'message');
    if (!message) {
      return { 
        error: 'Can not GET. Message with current id doesn`t exist'
      };
    } else {
      return {
        message: 'GET. Get message by id',
        data: message
      };
    }
  }

  static addMessage(message) {
    console.log(message);
    const { id } = message;
    
    if (Service.getMessage(id).data) {
      return { 
        error: 'Can not POST. Message with current id exists'
      };
    } else {
      Data.setData(message, './messagelist.json');
      return {
        message: 'POST. Create new message'
      };
    }
  }

  static updateMessage(message, updatedId) {
    const { id } = message
    if (updatedId != id) {
      return { 
        error: 'Can not PUT. Ids in the query string and in the object don`t match'
      };
    } else if (Service.getMessage(updatedId).error) {
      // Data.setData(user, './messagelist.json');
      // return {
      //   message: 'PUT. Create new message',
      //   code: 201
      // };
    } else {
      Data.updateData(message, './messagelist.json');
      return {
        message: 'PUT. Edit existing message',
        code: 200
      };
    }
  }

  static deleteMessage(id) {
    if (Service.getMessage(id).error) {
      return { 
        error: 'Can not DELETE. Message with current id doesn`t exist'
      };
    } else {
      Data.deleteData(id, './messagelist.json');
      return {
        message: 'DELETE. Delete existing message'
      };
    }
  };
}

module.exports = {
  Service
};
