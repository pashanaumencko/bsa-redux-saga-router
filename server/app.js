const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const messagesRouter = require('./routes/messages');


const app = express();

app.use(cors({origin: '*'}));
app.use(logger('dev'));
app.use(express.json());
// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((error, req, res, next) => {
    return res.status(400).send(`Incorrect request format`);
});

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/message', messagesRouter);

app.listen(8000);

module.exports = app;
