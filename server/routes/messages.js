const express = require('express');
const router = express.Router();

const { Service } = require("../services/service");
const { IsJSON } = require("../middlewares/middleware");

router.get('/', (req, res, next) => {
  const result = Service.getMessages();
  res.status(200).json(result.data);
});

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  const result = Service.getMessage(id);
  if (result.error) {
    res.status(404).send(result.error);
  } else {
    res.status(200).json(result.data);
  }
});

router.post('/', (req, res, next) => {
  const result = Service.addMessage(req.body);

  if (result.error) {
    console.log(result.error);
    res.status(400).send(result.error);
  } else {
    res.status(201).send(result.message);
  }
});

router.put('/:id', (req, res, next) => {
  const id = req.params.id;
  const result = Service.updateMessage(req.body, id);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(result.code).send(result.message);
  }
});

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const result = Service.deleteMessage(id);

  if (result.error) {
    res.status(400).send(result.error);   
  } else {
    res.status(200).send(result.message);
  }
});

module.exports = router;
